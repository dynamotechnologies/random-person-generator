const CHILD_AGE = 8;
const TEEN_AGE = 14;
const USA = 'USA'
const CHINA = 'China'
const PHILIPPINES = 'Philippines'
const MALE = 'Male';
const FEMALE = 'Female'
const RANDOM_PERSON_URL = 'https://randomuser.me/api/?nat=us'
const CHINA_LAST_NAMES = ["Wang", "Wong", "Lee", "Li", "Zhang", "Liu", "Chen", "Yang",
                            "Huang", "Zhao", "Wu", "Zhou", "Choi", "Sun", "Ma", "Lin",
							"Liang", "Song", "Feng", "Yu", "Wei", "Jiang", "Fan", "Shi",
							"Liao", "Fang", "Mao", "Yi", "Xu", "Hu", "He", "Lo", "Zheng",
							"Xie", "Tang", "Yuan"]
							
const CHINA_MALE_NAMES = ["Aiguo", "Bai", "Bingwen", "Bolin", "Changpu", "Cheng", "Chonglin",
                            "Chuanli", "Delun", "Dingxiang", "Duyi", "Fa", "Fengge", "Fu", "Gen",
							"Guangli", "Guang", "Gui", "Guowei", "Hong", "Huan", "Huiliang",
							"Jian", "Jiayi", "Kang", "Lei", "Ling", "Liwei", "Mingyu", "Niu", "Peng",
							"Qiao", "Qiqiang", "Renshu", "Shi", "Weimin", "Xiang", "Xiaobo",
							"Xiaoli", "Xing", "Yanlin", "Ying", "Yuanjun"]
							
const CHINA_FEMALE_NAMES = ["Baozhai", "Biyu", "Chunhua", "Cuifen", "Dongmei", "Daiyu", "Ehuang", 
                              "Fenfang", "Hualing", "Huifang", "Huiling", "Jiaying", "Jingfei", "Ju", 
							  "Lan", "Lanfen", "Lanying", "Lihua", "Luli", "Mei", "Meilin", "Mingxia", 
							  "Ning", "Peizhi", "Qiang", "Ruolan", "Suyin", "Ting", "Wenqian", "Xiaolian", 
							  "Xifeng", "Xue", "Yingtai", "Zhilan"]

const US_LAST_NAMES = ["Smith", "Johnson", "Williams", "Jones", "Brown", "Clark", "Lewis", "Walker",
                       "Young", "Adams", "Roberts", "Collins", "Rivera", "Reed", "Peterson", "Watson",
					   "Hughes", "Bryant", "Russel", "Griffin", "Hamilton", "Hayes", "Ellis", "Gibson",
					   "Murray", "Shaw", "Nichols", "Knight", "Bradley", "Montgomery", "Meyer"]
					 
const US_MALE_NAMES = ["James", "John", "Robert", "Michael", "William", "David", "Richard", "Joseph",
                       "Thomas", "Charles", "Christopher", "Daniel", "Matthew", "Anthony", "Donald",
					   "Mark", "Paul", "Steven", "Andrew", "Kenneth", "George", "Joshua", "Kevin",
					   "Brian", "Edward", "Ronald", "Timothy", "Jason", "Jeffrey", "Ryan", "Gary",
					   "Jacob", "Nicholas", "Eric", "Stephen", "Jonathan"]

const US_FEMALE_NAMES = ["Mary", "Patricia", "Jennifer", "Elizabeth", "Linda", "Barbara", "Susan",
                         "Susan", "Jessica", "Margaret", "Sarah", "Karen", "Nancy", "Betty", "Lisa",
						 "Sandra", "Ashley", "Kimberly", "Donna", "Carol", "Michelle", "Emily", "Amanda",
						 "Helen", "Melissa", "Deborah", "Stephanie", "Laura", "Rebecca", "Sharon",
						 "Cynthia", "Kathleen", "Amy", "Shirley", "Anna", "Angela", "Ruth", "Brenda",
						 "Pamela", "Nicole", "Katherine", "Virginia", "Catherine", "Christine", "Samantha"]
						 
const FIL_LAST_NAMES = ["Santos", "Reyes", "Cruz", "Bautista", "Ocampo", "Garcia", "Medoza", "Torres",
                        "Andrada", "Castillo", "Flores", "Villanueva", "Ramos", "Castro", "Rivera",
						"Aquino", "Navarro", "Salazar", "Mercado", "Chan", "Cheng", "Chua", "Dee",
						"Ku", "Lee", "Tan", "Tiu", "Ting", "Yap", "Yee", "Uytengsu", "Chotangco",
						"Chikiamco", "Yuchengco", "Uyboco", "Yutadco", "Dimaandal", "Dimaano",
						"Dimasalang", "Guinto", "Macaraeg", "Masipag", "Lumaban", "Pulumbarit", "Ablang",
						"Balason", "Calupid", "Dumo", "Lago"]
		
const FIL_MALE_NAMES = ["Joshua", "Christian", "Jerome", "Adrian", "Angelo", "John Carlo", "Jayson",
                        "John Rey", "Jaymes Thiago", "Jabez", "Jasper", "Dan", "Julez", "Emman",
						"Keran", "Julio", "Layton", "John", "Jake", "Melvin", "Francis", "Leon",
						"Hans", "Felix", "Clarence", "Grant", "Juriz", "Torkbik", "Sez", "Nobleza",
						"Cearo", "Jeremy", "Matthew", "Will", "Carlo", "Paul", "Christopher", "Anthony",
						"Ivan", "Gabriel", "Jian", "Jiayi", "Kang", "Lei", "Ling", "Liwei", "Mingyu", "Niu", "Peng"]
				
const FIL_FEMALE_NAMES = ["Maria", "Angela", "Hannah", "Bianca", "Camille", "Bea", "Grace", "Joyce",
                          "Angelica", "Trisha", "Mae", "Lorraine", "Loysa", "Angel", "Andrea", "Aya",
						  "Cha", "Elle", "Keziah", "Faye", "Aira", "Yana", "Danielle", "Kyla", "Nadine",
						  "Eirene", "Pia", "Lyn", "Cai", "Isabel", "Anj", "Mika", "Allie", "Tricia",
						  "Sophia", "Gabrielle", "Thea", "Eloise", "Amihan", "Amparo", "Abigail", "Belinda",
						  "Bituin", "Carmen", "Darlene", "Dolores", "Erlat", "Evelyn", "Qing Yuan",
						  "Qing", "Ning", "Peizhi", "Qiang", "Ruolan", "Suyin", "Ting"]




const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

const getRandomNDigitNumber = (numDigits) => Math.random().toString().slice(2, numDigits + 2);


const toTitleCase = (str) => str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());


const getRandomWeight = (age, sex) => {
	if (age < CHILD_AGE) return getRandomInt(40, 60);
	else if (age >= CHILD_AGE && age <= TEEN_AGE) return getRandomInt(60, 100);
	else if (age > TEEN_AGE) {
		if (sex === MALE) return getRandomInt(140, 190);
		else if (sex === FEMALE) return getRandomInt(100, 130);
	}
	else return -1;
}

const getRandomHeight = (age, sex) => {
	if (age < CHILD_AGE) return getRandomInt(40, 50);
	else if (age >= CHILD_AGE && age <= TEEN_AGE) {
		if (sex === MALE) return getRandomInt(50, 60);
		else if (sex === FEMALE) return getRandomInt(50, 58)
	}
	else if (age > TEEN_AGE) {
		if (sex === MALE) return getRandomInt(64, 74);
		else if (sex === FEMALE) return getRandomInt(60, 66);
	}
	else return -1;
}

const getRandomLetter = () => {
  	const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	return letters[getRandomInt(0, 25)]; 
}

const getPassportNumber = nationality => {
	switch(nationality) {
		case USA: return getRandomNDigitNumber(9);
		case CHINA: return 'G' + getRandomNDigitNumber(8);
		case PHILIPPINES: return getRandomLetter() + getRandomNDigitNumber(7) + getRandomLetter();
		default: return getRandomNDigitNumber(9);
	}
	
}

const getDriverId = nationality => getRandomNDigitNumber(8);

const getDateOfBirth = age => {
	const today = moment();
	const date = today.date();
	const month = today.month();
	const year = today.year();

	const birthDate = getRandomInt(1, 28);
	const birthMonth = getRandomInt(0, 11);
	let birthYear = year - age;
	if (birthDate > date && birthMonth > month) birthYear++;

	return moment().day(birthDate).month(birthMonth).year(birthYear).toISOString();
}

const getPhoneCountryCode = nationality => {
	switch(nationality) {
		case USA: return '+01';
		case CHINA: return '+86';
		case PHILIPPINES: return '+63';
		default: return '+01'; 
	}
}

const getPhoneNumber = nationality => {
	switch(nationality) {
		case USA: return getRandomNDigitNumber(3) + '-' + getRandomNDigitNumber(3) + '-' + getRandomNDigitNumber(4);
 		case CHINA: return '1' + getRandomNDigitNumber(2) + '-' + getRandomNDigitNumber(4) + '-' + getRandomNDigitNumber(4);
		case PHILIPPINES: return '09' + getRandomNDigitNumber(2) + '-' + getRandomNDigitNumber(7);
		default: return '182-423-9904'; 
	}
}

const buildProfile = (randomUser, inputArgs) => {
	const { nationality, sex, age, ethnicity, hairColor, eyeColor, hash } = inputArgs;
 	let { name: { first, last }, location: { street, city, state, postcode } } = randomUser;
 	console.log("nationality = ", nationality);
 	if (nationality !== USA) {
 		console.log("nationality is not USA");
		street = '';
		city = '';
		state = '';
		postcode = '';
 	}
	switch(nationality) {
		case USA  :  {
			switch(sex) {
				case MALE  : first = US_MALE_NAMES[getRandomInt(0, US_MALE_NAMES.length)]; break;
				case FEMALE: first = US_FEMALE_NAMES[getRandomInt(0, US_FEMALE_NAMES.length)]; break;
				default    : first = '';
			}
			
			last = US_LAST_NAMES[getRandomInt(0, US_LAST_NAMES.length)];
			break;
		}	
		case CHINA: {
			switch(sex) {
				case MALE  : first = CHINA_MALE_NAMES[getRandomInt(0, CHINA_MALE_NAMES.length)]; break;
				case FEMALE: first = CHINA_FEMALE_NAMES[getRandomInt(0, CHINA_FEMALE_NAMES.length)]; break;
				default    : first = '';
			}
			last = CHINA_LAST_NAMES[getRandomInt(0, CHINA_LAST_NAMES.length)];
			break;
		}
		case PHILIPPINES: {
			switch(sex) {
				case MALE  : first = FIL_MALE_NAMES[getRandomInt(0, FIL_MALE_NAMES.length)]; break;
				case FEMALE: first = FIL_FEMALE_NAMES[getRandomInt(0, FIL_FEMALE_NAMES.length)]; break;
				default    : first = '';
			}
			last = FIL_LAST_NAMES[getRandomInt(0, FIL_LAST_NAMES.length)];
			break;
		}
		default: {
			first = '';
			last = '';
		}
	}

	const profile = {
		firstName: toTitleCase(first) || '',
		lastName: toTitleCase(last) || '',
		age,
		sex,
		height: getRandomHeight(age, sex),
		weight: getRandomWeight(age, sex),
		hairColor: toTitleCase(hairColor),
		eyeColor: toTitleCase(eyeColor),
		ethnicity: toTitleCase(ethnicity),
		dateOfBirth: getDateOfBirth(age),
		nationality,
		passportNumber: getPassportNumber(nationality),
		additionalId: 'Drivers License',
		additionalIdNumber: getDriverId(nationality),
		streetAddress: toTitleCase(street) || '',
		city: toTitleCase(city) || '',
		state: toTitleCase(state) || '',
		postalCode: postcode || '',
		countryCode: getPhoneCountryCode(nationality),
		phoneNumber: getPhoneNumber(nationality),
		hash
	}

	console.log(JSON.stringify(profile, null, 4));
}

const getProfile = (nationality, sex, age, ethnicity, hairColor, eyeColor, hash) => {
	const inputArgs = { nationality, sex, age, ethnicity, hairColor, eyeColor, hash };
	const request = new Request(RANDOM_PERSON_URL);

	fetch(request)
		.then(resp => resp.json())
		.then(data => buildProfile(data.results[0], inputArgs));

}

