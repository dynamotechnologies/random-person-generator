# rename files to face.png
for f in $(find . -maxdepth 1 -mindepth 1 -type d);
	do echo $f; cd $f; mv $(find . -name 'face*') face.png; cd ..;
done

# rename files to fingerprint.png
for f in $(find . -maxdepth 1 -mindepth 1 -type d);
	do echo $f; cd $f; mv $(find . -name 'fingerprint*') fingerprint.png; cd ..;
done

