#!/bin/sh

# place in folder where files have been zipped
# run using one of the following on command line
# ./fabricLoader.sh china
# ./fabricLoader.sh usa
# ./fabricLoader.sh philippines

echo $1; 
echo $2;

if [ $1 = 'test' ]; then
	ENDPOINT='/org.country.citizen.Person'
elif [ $1 = 'demo' ]; then
	ENDPOINT='/org.agency.people.Person'
fi

if [ $2 = 'dhs' ]; then
	URL='http://ec2-13-57-27-84.us-west-1.compute.amazonaws.com:8080/api'
elif [ $2 = 'gfems' ]; then
	URL='http://ec2-54-193-121-12.us-west-1.compute.amazonaws.com:8080/api'
elif [ $2 = 'fcpd' ]; then
	URL='http://ec2-18-144-14-186.us-west-1.compute.amazonaws.com:8080/api'
fi


echo $URL

for f in $(find . -name '*.json'); 
	# do echo $URL$ENDPOINT
	do curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d @$f $URL$ENDPOINT;
done

